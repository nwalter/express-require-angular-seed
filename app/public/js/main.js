require.config({
    shim: {
        angular: {
            exports: "angular",
            deps: [
                "jquery"
            ]
        },
        ngRoute: {
            deps: [
                "angular"
            ]
        },
        underscore: {
            exports: "_"
        },
        bootstrap: {
            deps: [
                "jquery"
            ]
        }
    },
    baseUrl: "js/",
    paths: {
        requirejs: "../components/requirejs/require",
        angular: "../components/angular/angular",
        jquery: "../components/jquery/jquery",
        bootstrap: "../components/bootstrap/dist/js/bootstrap",
        underscore: "../components/underscore/underscore",
        domready: "../components/requirejs-domready/domReady",
        ngRoute: "../components/angular-route/angular-route",
        "requirejs-domready": "../components/requirejs-domready/domReady",
        "angular-route": "../components/angular-route/angular-route"
    },
    deps: [
        "/js/bootstrap.js"
    ]
});
