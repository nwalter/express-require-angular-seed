define([
    'angular',
    'ngRoute',
    'controllers/index',
    'directives/index',
    'filters/index',
    'services/index'
], function (angular) {
    'use strict';

    return angular.module('app', [
        'ngRoute',
        'app.controllers',
        'app.services',
        'app.filters',
        'app.directives'
    ]);
});
