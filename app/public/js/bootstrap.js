define([
    'require',
    'angular',
    'domready',
    'app',
    'routes'
], function (require, angular, domReady, app) {
    'use strict';

    domReady(function (document) {
        angular.bootstrap(document, ['app']);
        console.log('Bootstraping ok');
    });
});
