define([
    './module'
], function (module) {
    'use strict';

    return module.controller('rootController', [
        '$scope',
        function($scope) {
            console.log('Started root controller');

            $scope.title = 'Test the world!';
        }
    ]);
});
