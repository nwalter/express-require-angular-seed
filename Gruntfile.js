'use strict';

module.exports = function (grunt) {

    require('load-grunt-tasks')(grunt);
    require('time-grunt')(grunt);

    grunt.initConfig({
        express: {
            options: {
                port: 9000,
                script: 'app/app.js'
            },
            dev: {
            }
        },
        bower: {
            install: {
                rjsConfig: 'app/public/js/main.js'
            }
        },
        less: {
            all: {
                options: {
                    paths: ['app/public/css','app/public/components/bootstrap/less']
                },
                files: {
                    'app/public/css/app.css': 'app/public/css/*.less'
                }
            }
        },
        'cssmin': {
            'dist': {
                'src': ['app/public/css/app.css'],
                'dest': 'app/public/css/app.min.css'
            }
        },
        watch: {
            express: {
                files:  [ '**/*.js' ],
                tasks:  [ 'express:dev' ],
                options: {
                    spawn: false // Without this option specified express won't be reloaded
                }
            },
            less: {
                files: ['app/public/css/*.less'],
                tasks: [ 'less' ]
            }
        }
    });

    grunt.registerTask('server', [ 'less', 'express:dev', 'watch' ]);
    grunt.registerTask('dist', ['less','cssmin:dist']);
};
